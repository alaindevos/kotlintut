import javax.swing.JFrame
import javax.swing.JScrollPane
import javax.swing.JTable

class Data () {
    var data : Array<Array<String>> = arrayOf(arrayOf(""))
}

class Test internal constructor() {
    var f: JFrame
    var j: JTable
    init {
        f = JFrame()
        f.title = "JTable Example"
        val columnNames = arrayOf("Name", "Roll Number", "Department")
        var myData =Data() 
        dodata(myData)
        j = JTable( myData.data, columnNames)
        j.setBounds(30, 40, 200, 300)
        val sp = JScrollPane(j)
        f.add(sp)
        f.setSize(500, 200)
        f.isVisible = true
    }
    
    fun dodata( myData : Data ) {
        myData.data = arrayOf(arrayOf("Kundan Kumar Jha", "4031", "CSE"), arrayOf("Anand Jha", "6014", "IT"))
    }
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            Test()
        }
    }
}
