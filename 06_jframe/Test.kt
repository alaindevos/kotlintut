import javax.swing.JFrame
import javax.swing.JScrollPane
import javax.swing.JTable

class Test internal constructor() {
    // frame
    var f: JFrame

    // Table
    var j: JTable

    // Constructor
    init {
        // Frame initialization
        f = JFrame()

        // Frame Title
        f.title = "JTable Example"

        // Data to be displayed in the JTable
        val data = arrayOf(arrayOf("Kundan Kumar Jha", "4031", "CSE"), arrayOf("Anand Jha", "6014", "IT"))

        // Column Names
        val columnNames = arrayOf("Name", "Roll Number", "Department")

        // Initializing the JTable
        j = JTable(data, columnNames)
        j.setBounds(30, 40, 200, 300)

        // adding it to JScrollPane
        val sp = JScrollPane(j)
        f.add(sp)
        // Frame Size
        f.setSize(500, 200)
        // Frame Visible = true
        f.isVisible = true
    }

    companion object {
        // Driver  method
        @JvmStatic
        fun main(args: Array<String>) {
            Test()
        }
    }
}
