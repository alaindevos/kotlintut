import kotlin.math.PI

//Enum
enum  class Level {
    Low,Medium,High
}

// Sealed Class
sealed class Transport {
}
class Train(val line:String):Transport()
class Bus(val number:Int):Transport()

object JustOne {
  val n:Int=2
  fun f():Int=n*10
}

fun main(args: Array<String>) {
    println("Hello, World!")

    //class
    class Hamster {
        fun speak()="Squaek"
        fun exercise()= this.speak()+speak()+"Run on wheel"
    }
    val hamster=Hamster()
    println(hamster.exercise())

    //function&conversion
    fun fraction(num : Long,den : Long) =num.toDouble()/den 
    val num=1
    val den=2
    val f=fraction(num.toLong(),den.toLong())
    println(f)

    //string
    val s="ABcd"
    println(s.reversed())
    println(s.lowercase())

    //conversion
    val s2="123"
    println(s2.toInt())
    val i=123
    println(i.toString())

    //class
    val  r=IntRange(0,10)
    println(r.sum())

    //range
    for (i4 in 1..5){println("$i4")}

    //membership in range
    fun inNumRange(n:Int)= n in 50..100
    println("${inNumRange(75)}")

    //loop
    var i3=0
    do {
        print(".")
        i3+=1
    } while (i3<5)

    //for in
    for (c in "Kotlin") {
        print("$c")
    }

    //while
    var i4=0
    while (i4<4) {
        print(".")
        i4+=1
    }

    //string template
    val i5=123
    val s5="${i5}"
    println(s5)

    //if
    fun small(i:Int)=if (i<10) "small" else "large"
    println(small(5))

    //function
    fun square(i:Int):Int=i*i
    println(square(2))

    //unit
    fun printme(s:String):Unit=println(s)
    printme("Hi")

    //repeat
    repeat(3){println("OKIDOKI")}
    
    //char
    val s6:String="Alain"
    val c:Char=s6[0]
    println(c)

    //Class property
    class Cup {
        var percentFull=0
    }
    val c1=Cup()
    c1.percentFull=50
    println(c1.percentFull)

    //Member function
    class Cup2 {
        var percentFull=0
        fun add(increase:Int):Unit
            {
                percentFull+=increase
            }
    }
    var c22=Cup2()
    c22.add(30)
    print(c22.percentFull)

    //Constructor
    class Aname(var name:String){}
    var an:Aname=Aname("Alain")
    println("${an.name}")

    //Packages
    println(PI)

    //List
    val ints  = mutableListOf(1,2,3)
    for (i6 in ints){println (i6)}
    ints.add(4)
    ints.addAll(listOf(5,6))
    for (i6 in ints){println (i6)}

    //Map
    val c4 : MutableMap < String , Double >  = mutableMapOf( "Pi" to 3.141 , "e" to 2.718)
    for ((key,value) in c4) { println("$key $value") }
    println(c4["Pi"])
    c4.put("zero",0.0)
    try {
        var res = c4.getValue("Pi")
        println(res)
        res = c4.getValue("Alain")
    }
    catch (e: NoSuchElementException) {
       println("No Such Element")
    }

    //class getter & setter
    class MyClass {
        var i2 : Int = 0
        var i : Int = 0
        get() {
            var ret=field
            return ret 
        }
        set(value:Int) {
            field=value 
        }
        fun inc():Unit { 
            i2=i2+1
        }
    }
    val m1=MyClass()
    m1.i=1
    val rr=m1.i
    println(rr)

    //Constructor
    class Snake(id:String){
        var myid=id 
    }
    var s3:Snake=Snake("Alain")
    println(s3.myid)

    //Extension function
    fun  String.addAlain(): String {
        return this.plus("Alain")
    }
    println("Eddy".addAlain())

    //Default argument
    fun mystring(s:String ="Alain"):String {
        return s
    }
    println(mystring())
    println(mystring("Eddy"))

    //Named arguments
    fun addThree( i:Int ) : Int {
        return i+3 
    }
    println(addThree(i = 10))

    //when expressions
    fun int2string (i:Int):String {
        val xxx:String = when (i) {
            0 -> "Zero"
            1 -> "One"
            else -> "Other"
        }
        return xxx 
    }
    println(int2string(0))
    println(int2string(1))
    println(int2string(2))

    //Enum
    val lll=Level.Low 
    println(lll)

    //Data Class
    data class NNN (
        var firstName:String,
        var lastName:String 
    )
    val nnn=NNN("Alain","DeVos")
    nnn.firstName="Eddy"
    println(nnn.firstName)

    //Pair
    fun name(): Pair <String,String> {
        return Pair("Alain","Devos")
    }
    println(name().second)

    //Nullable
    val s7:String?=null 
    if(s7==null){
        println("Null")
    }

    //Safecall
    val s8=s7?.lowercase()
    if(s8==null){
        println("Null")
    }

    //Elvis operator, check if null or return leftside
    val s9:String = s7?:  "Null"
    println(s9) 

    //Lambda
    val mylist=listOf(1,2,3)
    val r3=mylist.map({n:Int -> n*n})
    println(r3)
    val r4=r3.filter{it  % 2 == 0}
    println(r4)
    var sum=0
    r4.forEach {sum += it}
    println(sum)

    //Recursion
    fun fact(n:Long):Long {
        if (n<=1) {return 1}
        else {return n*fact(n-1)}
    }
    println(fact(4))

    var counter=0
    //Complex constructor
    class CCC(i: Int){
        public val content:String 
        init {
            counter+=1
            content="$counter + $i"
        }
    }
    val c3=CCC(4)
    println(c3.content)

    //Secondary constructor
    class Sec ( i_ : Int, s_ : String ){
        var i : Int = i_  
        var s : String = s_ 
        init {
            println("Constructing")
        }
        constructor():this( 5, "" ){
        }
        constructor( s_ : String ):this(10,s_){            
        }
    }
    val c8 : Sec= Sec()
    println(c8.i)

    //Inheritance
    open class Animal{
        open val name="Animal"
        open fun myname():String { return "Animal_" + name 
        } 

    }
    class Dog:Animal(){
        val isay="Woef"
        override val name="Dog"
        override fun myname():String = "Dog_" + name
    }
    class Cat:Animal(){
        val isay="Miaouw"
        override val name="Cat"
        override fun myname():String = "Cat_" + name
    }
    val d=Dog()
    println(d.name)
    println(d.isay)

    //Upcasting&polymorphism
    val a:Animal=d
    println(a.myname()) // dog function

    //Downcasting
    val d2:Dog= a as Dog 
    println(d2.name)

    //is
    fun printifdog (a:Animal){
        if(a is Dog)
        println("Is Dog")
    }

    val t2:Transport=Train("Gent")
    fun printtransport(t:Transport){
        when (t) {
            is Train -> println(t.line)
            is Bus -> println(t.number)
        }
    }
    printtransport(t2)
    
    //Object
    println(JustOne.f())

    //Exceptions
    fun test(code:Int){
        if(code < 100)
            throw IllegalArgumentException ("Error")
    }
    try {
        test(10)
    }
    catch (e:IllegalArgumentException) {
        println(e.message)
    }
    finally {
        println("Run Always")
    }

    //Operator overloading
    data class Num(val n:Int) {
        operator fun plus(rval:Num):Num {
            return Num(n+rval.n)
        }
        operator fun unaryMinus():Num {
            return Num(-n)
        }
    }
    val n8:Num=Num(1)+Num(2)
    println(n8.n)
    println((-Num(5)).n)
    println("Done")
}
