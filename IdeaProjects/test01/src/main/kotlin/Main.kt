import java.sql.Connection
import java.sql.DriverManager

fun main(args: Array<String>) {
    println("Hello World!")

    // Try adding program arguments via Run/Debug configuration.
    // Learn more about running applications: https://www.jetbrains.com/help/idea/running-applications.html.
    println("Program arguments: ${args.joinToString()}")

    println("Hi From Mysql")
    run {
        println("Starting")
        println("\n\n***** MySQL JDBC Connection Testing *****")
        var conn: Connection? = null
        try {
            Class.forName("org.mariadb.jdbc.Driver")
            val userName = "root"
            val password = "root"
            val url = "jdbc:mariadb://127.0.0.1/root"
            conn = DriverManager.getConnection(url, userName, password)
            println("\nDatabase Connection Established...")
        } catch (ex: Exception) {
            System.err.println("Cannot connect to database server")
            ex.printStackTrace()
        }
        try {
            val query = "SELECT * FROM names"
            // create the java statement
            val st = conn!!.createStatement()
            // execute the query, and get a java resultset
            val rs = st.executeQuery(query)
            // iterate through the java resultset
            while (rs.next()) {
                val firstName = rs.getString("firstname")
                val lastName = rs.getString("lastname")
                // print the results
                System.out.format("%s, %s \n", firstName, lastName)
            }
            st.close()
        } catch (e: Exception) {
            System.err.println("Got an exception! ")
            System.err.println(e.message)
        }
        if (conn != null) {
            try {
                println("\n***** Let terminate the Connection *****")
                conn.close()
                println("\nDatabase connection terminated...")
            } catch (ex: Exception) {
                println("Error in connection termination!")
            }
        }
        println("Stopping")
    }


}