import java.sql.Connection
import java.sql.DriverManager

fun main(args: Array<String>) {
    println("Starting")
    println("\n\n***** MySQL JDBC Connection Testing *****")
    var conn: Connection? = null

    try {
        Class.forName("org.mariadb.jdbc.Driver")
        val userName = "root"
        val password = "root"
        val url = "jdbc:mariadb://127.0.0.1/syslogng"
        conn = DriverManager.getConnection(url, userName, password)
        println("\nDatabase Connection Established...")
    }   
    catch (ex: Exception) {
        System.err.println("Cannot connect to database server")
        ex.printStackTrace()
    }

    try {
        val query = "SELECT datetime,program,message FROM messages_zorin_20230602 order by datetime"
        val st = conn!!.createStatement()
        val rs = st.executeQuery(query)
        while (rs.next()) {
            val datetime = rs.getString("datetime")
            val program = rs.getString("program")
            val message = rs.getString("message")
            System.out.format("| %s | %20s | %s |\n", datetime,program,message)
        }
        st.close()
    }   
    catch (e: Exception) {
        System.err.println("Got an exception! ")
        System.err.println(e.message)
    }

    if (conn != null) {
        try {
            println("\n***** Let terminate the Connection *****")
            conn.close()
            println("\nDatabase connection terminated...")
        }   
        catch (ex: Exception) {
            println("Error in connection termination!")
        }
        println("Stopping")
    }
}
