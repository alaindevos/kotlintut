import javax.swing.JFrame
import javax.swing.JScrollPane
import javax.swing.JTable
import java.sql.Connection
import java.sql.DriverManager

class Data () {
    var data : ArrayList<Array<String>> = arrayListOf(arrayOf(""))
}

class Test internal constructor() {
    var f: JFrame
    var j: JTable
    init {
        f = JFrame()
        f.title = "JTable Example"
        val columnNames = arrayOf("DateTime", "Program", "Message")
        var myData =Data() 
        dodata(myData)
        // var data2=arrayOf(arrayOf("A","B","1"),arrayOf("C","D","2"))
        var len=myData.data.size
        val m=300
        if (len > m) { len = m}
        var data3=myData.data.subList(1,len).toTypedArray()
        j = JTable( data3, columnNames)
        j.setBounds(20, 60, 800)
        val sp = JScrollPane(j)
        f.add(sp)
        f.setSize(500, 200)
        f.isVisible = true
    }
    
    fun dodata( myData : Data ) {
        println("Starting")
        println("\n\n***** MySQL JDBC Connection Testing *****")
        var conn: Connection? = null
    
        try {
            Class.forName("org.mariadb.jdbc.Driver")
            val userName = "root"
            val password = "root"
            val url = "jdbc:mariadb://127.0.0.1/syslogng"
            conn = DriverManager.getConnection(url, userName, password)
            println("\nDatabase Connection Established...")
        }   
        catch (ex: Exception) {
            System.err.println("Cannot connect to database server")
            ex.printStackTrace()
        }
    
        try {
            val query = "SELECT datetime,program,message FROM messages_zorin_20230602 order by datetime desc"
            val st = conn!!.createStatement()
            val rs = st.executeQuery(query)
            while (rs.next()) {
                val datetime = rs.getString("datetime")
                val program = rs.getString("program")
                val message = rs.getString("message")
                System.out.format("| %s | %20s | %s |\n", datetime,program,message)
                myData.data.add(arrayOf(datetime, program , message ))

            }
            st.close()
        }   
        catch (e: Exception) {
            System.err.println("Got an exception! ")
            System.err.println(e.message)
        }
    
        if (conn != null) {
            try {
                println("\n***** Let terminate the Connection *****")
                conn.close()
                println("\nDatabase connection terminated...")
            }   
            catch (ex: Exception) {
                println("Error in connection termination!")
            }
            println("Stopping")
        }
    
    }
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            Test()
        }
    }
}
