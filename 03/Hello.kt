
sealed class Shape{  
    class Circle(var radius: Float): Shape()  
    class Rectangle(var length: Int, var breadth: Int): Shape()  
}  

fun main() {
    //arrays  
    var mystrings = Array <String> (size = 5, init= { _ -> "" })
    mystrings.set(2,"Alain")
    println(mystrings.get(2))
    for(s : String  in mystrings) {println(s)}

    //lambdas
    val myaddition : ( Int , Int ) -> Int  = {  a,b -> a + b }
    println(myaddition(2,3))

    // sealed class
    fun eval(e: Shape) =  
        when (e) {  
            is Shape.Circle ->println("Circle area is ${3.14*e.radius*e.radius}")  
            is Shape.Rectangle ->println("Rectagle area is ${e.length*e.breadth}")  
        }  
    var circle = Shape.Circle(5.0f)  
    var rectangle = Shape.Rectangle(4,5)  
    eval(circle)  
    eval(rectangle)  

    //class
    open class Base {
        val x: Int = 10
    }
    class Derived : Base(){
        fun printme() {
            println (x)
        }
    }
    val d : Derived=Derived()
    d.printme()

    //Collection
    val  list= listOf("Alain",2,3.0)
    println(list[0])
    println(list[1])

    val map=mapOf(Pair("DeVos","Alain"),Pair("DeWolf","Eddy"))
    println(map["DeWolf"])

    val set=setOf(1,3,5);
    println(set)

}
